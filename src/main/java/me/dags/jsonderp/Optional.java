package me.dags.jsonderp;

/**
 * @author dags_ <dags@dags.me>
 */

public class Optional<T>
{
    private static final Optional EMPTY = new Optional(null);

    private final T t;

    private Optional(T value)
    {
        t = value;
    }

    public boolean isPresent()
    {
        return t != null;
    }

    public T get()
    {
        return t;
    }

    @SuppressWarnings("unchecked")
    public <V> V asParent(Class<? super T> superType)
    {
        return (V) superType.cast(t);
    }

    public static <T> Optional<T> of(T value)
    {
        if (value == null)
        {
            return empty();
        }
        else
        {
            return new Optional<T>(value);
        }
    }

    public static <T> Optional<T> empty()
    {
        return Optional.EMPTY;
    }
}
