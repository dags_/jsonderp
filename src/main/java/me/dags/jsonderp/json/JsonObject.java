/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.json;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonObject extends JsonElement
{
    private Map<String, JsonElement> elements = new LinkedHashMap<String, JsonElement>();

    public Map<String, JsonElement> getElementMap()
    {
        return new LinkedHashMap<String, JsonElement>(elements);
    }

    public JsonObject add(String key, JsonElement element)
    {
        elements.put(key, element);
        return this;
    }

    public JsonElement get(String key)
    {
        return elements.get(key);
    }

    public JsonObject remove(String key)
    {
        if (contains(key))
        {
            elements.remove(key);
        }
        return this;
    }

    public boolean contains(String key)
    {
        return elements.containsKey(key);
    }

    public boolean contains(String... keys)
    {
        for (String key : keys)
        {
            if (!contains(key))
            {
                return false;
            }
        }
        return true;
    }

    public String matchTypedField(String name)
    {
        for (String s : elements.keySet())
        {
            if (s.endsWith(name) && s.startsWith("@") && s.contains(">"))
            {
                return s;
            }
        }
        return name;
    }

    public int size()
    {
        return elements.size();
    }

    @Override
    public JsonElement.Type getType()
    {
        return JsonElement.Type.OBJECT;
    }

    @Override
    public JsonArray asJsonArray()
    {
        return null;
    }

    @Override
    public JsonObject asJsonObject()
    {
        return this;
    }

    @Override
    public JsonValue asJsonValue()
    {
        return null;
    }

    @Override
    public String toJson()
    {
        StringBuilder sb = new StringBuilder().append("{");
        for (String key : elements.keySet())
        {
            sb.append("\"").append(key).append("\":").append(elements.get(key).toJson()).append(",");
        }
        return sb.deleteCharAt(sb.length() - 1).append("}").toString();
    }

    @Override
    public int hashCode()
    {
        return toJson().hashCode();
    }
}
