/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.json;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonValue extends JsonElement
{
    private final Object value;

    public JsonValue(Object value)
    {
        this.value = value;
    }

    public String asString()
    {
        return unEscapeString(value.toString());
    }

    public Number asNumber()
    {
        return (Number) value;
    }

    public int asInt()
    {
        return asNumber().intValue();
    }

    public short asShort()
    {
        return asNumber().shortValue();
    }

    public long asLong()
    {
        return asNumber().longValue();
    }

    public double asDouble()
    {

        return asNumber().doubleValue();
    }

    public float asFloat()
    {
        return asNumber().floatValue();
    }

    public boolean asBoolean()
    {
        return Boolean.valueOf(toString());
    }

    public Object asObject()
    {
        return value;
    }

    public boolean isInstance(Class<?> c)
    {
        return c.isInstance(value);
    }

    @Override
    public JsonElement.Type getType()
    {
        return JsonElement.Type.VALUE;
    }

    @Override
    public JsonArray asJsonArray()
    {
        return null;
    }

    @Override
    public JsonObject asJsonObject()
    {
        return null;
    }

    @Override
    public JsonValue asJsonValue()
    {
        return this;
    }

    @Override
    public String toJson()
    {
        return isPresent() ? jsonString() : "null";
    }

    public boolean isPresent()
    {
        return value != null;
    }

    @Override
    public int hashCode()
    {
        return asString().hashCode();
    }

    private String jsonString()
    {
        return isInstance(String.class) ? "\"" + escapeString(asString()) + "\"" : asString();
    }

    protected static String escapeString(String s)
    {
        StringBuilder b = new StringBuilder();
        for (char c : s.toCharArray())
        {
            if (c >= 128 || c == '"' || c == '\\')
                b.append("\\u").append(String.format("%04X", (int) c));
            else
                b.append(c);
        }
        return b.toString();
    }

    protected static String unEscapeString(String st)
    {
        StringBuilder sb = new StringBuilder(st.length());
        for (int i = 0; i < st.length(); i++)
        {
            char ch = st.charAt(i);
            if (ch == '\\')
            {
                char nextChar = (i == st.length() - 1) ? '\\' : st.charAt(i + 1);
                if (nextChar >= '0' && nextChar <= '7')
                {
                    String code = "" + nextChar;
                    i++;
                    if ((i < st.length() - 1) && st.charAt(i + 1) >= '0' && st.charAt(i + 1) <= '7')
                    {
                        code += st.charAt(i + 1);
                        i++;
                        if ((i < st.length() - 1) && st.charAt(i + 1) >= '0' && st.charAt(i + 1) <= '7')
                        {
                            code += st.charAt(i + 1);
                            i++;
                        }
                    }
                    sb.append((char) Integer.parseInt(code, 8));
                    continue;
                }
                switch (nextChar)
                {
                    case '\\':
                        ch = '\\';
                        break;
                    case 'b':
                        ch = '\b';
                        break;
                    case 'f':
                        ch = '\f';
                        break;
                    case 'n':
                        ch = '\n';
                        break;
                    case 'r':
                        ch = '\r';
                        break;
                    case 't':
                        ch = '\t';
                        break;
                    case '\"':
                        ch = '\"';
                        break;
                    case '\'':
                        ch = '\'';
                        break;
                    case 'u':
                        if (i >= st.length() - 5)
                        {
                            ch = 'u';
                            break;
                        }
                        int code = Integer.parseInt("" + st.charAt(i + 2) + st.charAt(i + 3) + st.charAt(i + 4) + st.charAt(i + 5), 16);
                        sb.append(Character.toChars(code));
                        i += 5;
                        continue;
                }
                i++;
            }
            sb.append(ch);
        }
        return sb.toString();
    }
}
