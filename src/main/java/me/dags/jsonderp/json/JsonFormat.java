package me.dags.jsonderp.json;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonFormat
{
    public static final JsonFormat COMPACT = new JsonFormat(0, 0, false);
    public static final JsonFormat NORMAL = new JsonFormat(0, 1, false);
    public static final JsonFormat PRETTY_FORMAT = new JsonFormat(4, 1, true);

    private String indent = "";
    private String space = "";
    private String lineBreak = "";

    protected JsonFormat(int indents, int spaces, boolean lineBreaks)
    {
        while (spaces > 0)
        {
            space += " ";
            spaces--;
        }
        while (indents > 0)
        {
            indent += space;
            indents--;
        }
        lineBreak = lineBreaks ? "\n" : "";
    }

    public String format(String json)
    {
        StringBuilder sb = new StringBuilder();
        char[] chars = json.toCharArray();
        int depth = 0;
        char previous = (char) -1;
        for (int i = 0, length = chars.length; i < length; i++)
        {
            if (previous == ',' || previous == ':')
                sb.append(space);
            char c = chars[i];
            switch (c)
            {
                case ' ':
                    break;
                case '"':
                    boolean escape = false;
                    sb.append(c);
                    while (i < length)
                    {
                        c = chars[++i];
                        if (!escape && c == '"')
                            break;
                        escape = c == '\\';
                        sb.append(c);
                    }
                    sb.append(c);
                    break;
                case ':':
                    sb.append(c).append(space);
                    break;
                case ',':
                    sb.append(c);
                    newLine(sb, depth);
                    indent(sb, depth);
                    break;
                case '{':
                    sb.append(c);
                    if (i < length - 1 && chars[i + 1] == '}')
                    {
                        c = chars[++i];
                        sb.append(c);
                    }
                    else
                    {
                        depth++;
                        newLine(sb, depth);
                        indent(sb, depth);
                    }
                    break;
                case '}':
                    newLine(sb, depth);
                    depth--;
                    indent(sb, depth);
                    sb.append(c);
                    break;
                case '[':
                    sb.append(c);
                    if (i < length - 1 && chars[i + 1] == ']')
                    {
                        c = chars[++i];
                        sb.append(c);
                    }
                    else
                    {
                        depth++;
                        newLine(sb, depth);
                        indent(sb, depth);
                    }
                    break;
                case ']':
                    newLine(sb, depth);
                    depth--;
                    indent(sb, depth);
                    sb.append(c);
                    break;
                default:
                    sb.append(c);
            }
            previous = sb.charAt(sb.length() - 1);
        }
        return sb.toString();
    }

    private StringBuilder newLine(StringBuilder sb, int depth)
    {
        if (depth > 0)
        {
            sb.append(lineBreak);
        }
        return sb;
    }

    private StringBuilder indent(StringBuilder sb, int depth)
    {
        while (depth > 0)
        {
            sb.append(indent);
            depth--;
        }
        return sb;
    }
}
