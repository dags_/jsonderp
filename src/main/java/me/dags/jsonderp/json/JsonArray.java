/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.json;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonArray extends JsonElement
{
    private List<JsonElement> elements = new ArrayList<JsonElement>();

    public List<JsonElement> getElements()
    {
        return new ArrayList<JsonElement>(elements);
    }

    public JsonArray add(JsonElement element)
    {
        elements.add(element);
        return this;
    }

    public JsonArray add(JsonElement... jsonElements)
    {
        for (JsonElement e : jsonElements)
        {
            add(e);
        }
        return this;
    }

    public JsonElement getAt(int index)
    {
        if (index < elements.size())
        {
            return elements.get(index);
        }
        return null;
    }

    public boolean removeAt(int index)
    {
        if (index < getElements().size())
        {
            elements.remove(index);
            return true;
        }
        return false;
    }

    public boolean remove(JsonElement element)
    {
        boolean result = false;
        int index = 0;
        for (JsonElement e : getElements())
        {
            if (e.toString().equals(element.toString()))
            {
                result = true;
                elements.remove(index);
            }
            index++;
        }
        return result;
    }

    public int length()
    {
        return elements.size();
    }

    @Override
    public JsonElement.Type getType()
    {
        return JsonElement.Type.ARRAY;
    }

    @Override
    public JsonArray asJsonArray()
    {
        return this;
    }

    @Override
    public JsonObject asJsonObject()
    {
        return null;
    }

    @Override
    public JsonValue asJsonValue()
    {
        return null;
    }

    @Override
    public String toJson()
    {
        if (elements.isEmpty())
        {
            return "[]";
        }
        StringBuilder sb = new StringBuilder().append("[");
        for (JsonElement e : elements)
        {
            sb.append(e.toJson()).append(",");
        }
        return sb.deleteCharAt(sb.length() - 1).append("]").toString();
    }

    @Override
    public int hashCode()
    {
        return toJson().hashCode();
    }
}
