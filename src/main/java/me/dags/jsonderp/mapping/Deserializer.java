/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.mapping;

import me.dags.jsonderp.annotation.Json;
import me.dags.jsonderp.annotation.JsonSerializeType;
import me.dags.jsonderp.json.JsonElement;
import me.dags.jsonderp.json.JsonObject;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author dags_ <dags@dags.me>
 */

public class Deserializer
{
    public <T> T deserialize(JsonObject json, Class<T> target) throws ReflectiveOperationException
    {
        return constructJavaObject(target, json);
    }

    private <T> T constructJavaObject(Class<T> target, JsonObject json) throws ReflectiveOperationException
    {
        try
        {
            Constructor c = target.getDeclaredConstructor();
            if (c.isAccessible())
            {
                T object = target.newInstance();
                populateJavaObject(object, json.asJsonObject());
                return object;
            }
            else
            {
                c.setAccessible(true);
                Object object = c.newInstance();
                c.setAccessible(false);
                populateJavaObject(object, json.asJsonObject());
                return target.cast(object);
            }
        }
        catch (NoSuchMethodException e)
        {
            String c = target.getName();
            String err = "JsonDerp requires " + c + ".class to implement a parameterless constructor: '" + c + "()'";
            e.initCause(new Throwable(err));
            throw e;
        }
    }

    private void populateJavaObject(Object object, JsonObject json) throws ReflectiveOperationException
    {
        Class<?> c = object.getClass();
        do
        {
            for (Field f : c.getDeclaredFields())
            {
                boolean access = f.isAccessible();
                f.setAccessible(true);
                Class<?> type = f.getType();
                Json jsonMeta = f.getAnnotation(Json.class);
                JsonSerializeType typeMeta = f.getAnnotation(JsonSerializeType.class);
                if ((type.isInterface() && typeMeta == null) || (type.isArray() && type.getComponentType().isInterface()))
                {
                    continue;
                }
                String name = jsonMeta != null ? jsonMeta.name() : f.getName();
                name = typeMeta != null ? json.matchTypedField(name) : name;
                if (json.contains(name) && !f.getType().isAssignableFrom(Map.class))
                {
                    if (typeMeta != null && name.startsWith("@") && name.contains(">"))
                    {
                        String trimmed = name.substring(1, name.indexOf(">"));
                        type = Class.forName(trimmed);
                    }
                    JsonElement element = json.get(name);
                    Object fieldObject = getAsJavaObject(element, type);
                    f.set(object, fieldObject);
                    f.setAccessible(access);
                }
            }
            c = c.getSuperclass();
        }
        while (c != null);
    }

    @SuppressWarnings("unchecked")
    private Object getAsJavaObject(JsonElement element, Class<?> type) throws ReflectiveOperationException
    {
        if (element.getType() == JsonElement.Type.OBJECT)
        {
            JsonObject jsonObject = element.asJsonObject();
            return constructJavaObject(type, jsonObject);
        }
        else if (element.getType() == JsonElement.Type.ARRAY)
        {
            List<JsonElement> elementList = element.asJsonArray().getElements();
            List<Object> objectList = new ArrayList<Object>(elementList.size());
            boolean isArray = type.isArray();
            type = isArray ? type.getComponentType() : type;
            for (JsonElement e : elementList)
            {
                objectList.add(getAsJavaObject(e, type));
            }
            if (isArray)
                return toArrayOf(objectList, type);
            return objectList;
        }
        else
        {
            Object object = element.asJsonValue().asObject();
            if (Number.class.isInstance(object))
            {
                NumberType numberType = NumberType.getType(type);
                object = numberType.cast(object);
            }
            if (Enum.class.isAssignableFrom(type))
            {
                object = Enum.valueOf((Class<Enum>) type, object.toString().toUpperCase());
            }
            return object;
        }
    }

    protected static Object toArrayOf(List<Object> list, Class<?> type)
    {
        Object array = Array.newInstance(type, list.size());
        for (int i = 0, n = list.size(); i < n; i++)
        {
            Array.set(array, i, list.get(i));
        }
        return array;
    }
}
