/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.mapping;

import me.dags.jsonderp.annotation.Json;
import me.dags.jsonderp.annotation.JsonIgnore;
import me.dags.jsonderp.annotation.JsonSerializeType;
import me.dags.jsonderp.json.JsonArray;
import me.dags.jsonderp.json.JsonElement;
import me.dags.jsonderp.json.JsonObject;
import me.dags.jsonderp.json.JsonValue;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Map;

/**
 * @author dags_ <dags@dags.me>
 */

public class Serializer
{
    public JsonElement serialize(Object object) throws ReflectiveOperationException
    {
        return toElement(object);
    }

    private JsonElement toElement(Object object) throws ReflectiveOperationException
    {
        if (object == null || String.class.isInstance(object) || Number.class.isInstance(object) || Boolean.class.isInstance(object))
        {
            return new JsonValue(object);
        }
        if (Enum.class.isInstance(object))
        {
            return new JsonValue(object.toString());
        }
        if (Map.class.isAssignableFrom(object.getClass()))
        {
            return new JsonValue(null);
        }
        if (List.class.isAssignableFrom(object.getClass()) || object.getClass().isArray())
        {
            return mapArray(object);
        }
        return mapObject(object, object.getClass());
    }

    private JsonElement mapObject(Object object, Class<?> c) throws ReflectiveOperationException
    {
        JsonObject jsonObject = new JsonObject();
        do
        {
            for (Field f : c.getDeclaredFields())
            {
                if (!Modifier.isTransient(f.getModifiers()))
                {
                    boolean access = f.isAccessible();
                    f.setAccessible(true);
                    Class<?> type = f.getType();
                    Json jsonMeta = f.getAnnotation(Json.class);
                    JsonIgnore ignoreMeta = f.getAnnotation(JsonIgnore.class);
                    JsonSerializeType typeMeta = f.getAnnotation(JsonSerializeType.class);
                    if (ignoreMeta != null || (type.isInterface() && (type.isArray() || typeMeta == null)))
                    {
                        continue;
                    }
                    String classMeta = typeMeta != null ? getTypeAnnotation(f, object) : "";
                    String name = jsonMeta != null ? classMeta + jsonMeta.name() : classMeta + f.getName();
                    Object fieldValue = f.get(object);
                    jsonObject.add(name, toElement(fieldValue));
                    f.setAccessible(access);
                }
            }
            c = c.getSuperclass();
        }
        while (c != null);
        return jsonObject;
    }

    @SuppressWarnings("unchecked")
    private JsonElement mapArray(Object object) throws ReflectiveOperationException
    {
        JsonArray json = new JsonArray();
        if (object.getClass().isArray())
        {
            for (Object o : toObjectArray(object))
            {
                json.add(toElement(o));
            }
        }
        else
        {
            List<Object> toList = (List<Object>) object;
            for (Object o : toList)
            {
                json.add(toElement(o));
            }
        }
        return json;
    }

    private static String getTypeAnnotation(Field f, Object o) throws IllegalAccessException
    {
        Class fieldType = f.get(o).getClass();
        if (fieldType.getEnclosingClass() != null)
        {
            return  "@" + fieldType.getEnclosingClass().getCanonicalName() + "$" + fieldType.getSimpleName() + ">";
        }
        return "@" + fieldType.getCanonicalName() + ">";
    }

    protected static Object[] toObjectArray(Object inArray)
    {
        int length = Array.getLength(inArray);
        Object[] array = new Object[length];
        for (int i = 0; i < length; i++)
        {
            array[i] = Array.get(inArray, i);
        }
        return array;
    }
}
