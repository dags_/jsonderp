/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.parsing;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonReader
{
    private final char[] buffer = new char[8192];
    private final char[] chars;
    private final int length;
    private char current;
    private int pos;
    private int mark;

    public JsonReader(String s)
    {
        chars = s.toCharArray();
        length = chars.length - 1;
        pos = 0;
        current = chars[0];
    }

    public boolean hasNext()
    {
        return pos < length;
    }

    public char read()
    {
        return current = chars[++pos];
    }

    public char current()
    {
        return current;
    }

    private void updateCurrent()
    {
        if (pos < length)
        {
            current = chars[pos];
        }
        else
        {
            current = chars[length - 1];
        }
    }

    public void skip(int amount)
    {
        pos += amount;
        updateCurrent();
    }

    public void mark()
    {
        mark = pos;
    }

    public void reset()
    {
        pos = mark;
        updateCurrent();
    }

    public String readString()
    {
        int buffPos = 0;
        boolean escape = false;
        while (hasNext())
        {
            char c = read();
            if (!escape && c == '"')
                break;
            escape = c == '\\';
            buffer[buffPos++] = c;
        }
        return new String(buffer, 0, buffPos);
    }

    public Number readNumber()
    {
        mark();
        boolean isDecimal = false;
        int buffPos = 0;
        while (hasNext())
        {
            char c = current();
            if (JsonReader.isNumberChar(c))
            {
                if (c == '.')
                    isDecimal = true;
                buffer[buffPos++] = c;
                mark();
                skip(1);
            }
            else
            {
                reset();
                break;
            }
        }
        if (isDecimal)
            return Double.parseDouble(new String(buffer, 0, buffPos));
        return Long.parseLong(new String(buffer, 0, buffPos));
    }

    protected static boolean isNumberChar(char c)
    {
        return Character.isDigit(c) || c == '.' || c == '-' || c == '+' || c == 'e' || c == 'E';
    }
}
