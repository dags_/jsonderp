/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp.parsing;

import me.dags.jsonderp.json.JsonArray;
import me.dags.jsonderp.json.JsonElement;
import me.dags.jsonderp.json.JsonObject;
import me.dags.jsonderp.json.JsonValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author dags_ <dags@dags.me>
 */

public class JsonParser
{
    public JsonElement parse(String jsonString)
    {
        return readValue(new JsonReader(jsonString));
    }

    public JsonElement parse(InputStream inputStream) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (reader.ready())
        {
            sb.append(reader.readLine());
        }
        return parse(sb.toString());
    }

    private JsonArray readArray(JsonReader reader)
    {
        JsonArray array = new JsonArray();
        while (reader.hasNext())
        {
            char c = reader.read();
            switch (c)
            {
                case ',':
                case '[':
                case ' ':
                case 10:
                    break;
                case ']':
                    return array;
                default:
                    array.add(readValue(reader));
            }
        }
        return array;
    }

    private JsonObject readObject(JsonReader reader)
    {
        JsonObject object = new JsonObject();
        JsonElement read = null;
        String key = null;
        while (reader.hasNext())
        {
            char c = reader.read();
            switch (c)
            {
                case ' ':
                case 10:
                    break;
                case ':':
                    if (read != null)
                        key = read.asJsonValue().asString();
                    break;
                case ',':
                    object.add(key, read);
                    break;
                case '}':
                    object.add(key, read);
                    return object;
                default:
                    read = readValue(reader);
            }
        }
        return object;
    }

    private JsonElement readValue(JsonReader reader)
    {
        if (reader.hasNext())
        {
            char c = reader.current();
            switch (c)
            {
                case '{':
                    return readObject(reader);
                case '[':
                    return readArray(reader);
                case 'f':
                case 'F':
                    reader.skip(4);
                    return new JsonValue(false);
                case 'n':
                    reader.skip(3);
                    return new JsonValue(null);
                case 't':
                case 'T':
                    reader.skip(3);
                    return new JsonValue(true);
                case '"':
                    return new JsonValue(reader.readString());
                default:
                    if (JsonReader.isNumberChar(c))
                        return new JsonValue(reader.readNumber());
            }
            reader.skip(1);
            return readValue(reader);
        }
        return new JsonValue(null);
    }
}
