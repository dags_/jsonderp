/*
 * Copyright (c) 2015, dags_ <dags@dags.me>
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 */

package me.dags.jsonderp;

import me.dags.jsonderp.json.JsonElement;
import me.dags.jsonderp.json.JsonFormat;
import me.dags.jsonderp.json.JsonObject;
import me.dags.jsonderp.mapping.Deserializer;
import me.dags.jsonderp.mapping.Serializer;
import me.dags.jsonderp.parsing.JsonParser;

import java.io.*;
import java.net.URL;

/**
 * @author dags_ <dags@dags.me>
 */

public final class JsonDerp
{
    public static final String VERSION = "1.3.0";
    private static final JsonParser JSON_PARSER = new JsonParser();
    private static final Serializer SERIALIZER = new Serializer();
    private static final Deserializer DESERIALIZER = new Deserializer();

    public static JsonElement parse(String jsonString)
    {
        return JSON_PARSER.parse(jsonString);
    }

    public static Optional<JsonElement> parse(InputStream inputStream)
    {
        try
        {
            JsonElement element = JSON_PARSER.parse(inputStream);
            return Optional.of(element);
        }
        catch (IOException e)
        {
            return Optional.empty();
        }
    }

    public static Optional<JsonElement> parse(File file)
    {
        try
        {
            return parse(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            return Optional.empty();
        }
    }

    public static Optional<JsonElement> parse(URL url)
    {
        try
        {
            return parse(url.openStream());
        }
        catch (IOException e)
        {
            return Optional.empty();
        }
    }

    public static Optional<JsonElement> serialize(Object object)
    {
        try
        {
            return Optional.of(SERIALIZER.serialize(object));
        }
        catch (ReflectiveOperationException e)
        {
            return Optional.empty();
        }
    }

    public static <T> Optional<T> deserialize(JsonObject jsonObject, Class<T> type)
    {
        try
        {
            return Optional.of(DESERIALIZER.deserialize(jsonObject, type));
        }
        catch (ReflectiveOperationException e)
        {
            return null;
        }
    }

    public static <T> Optional<T>  deserialize(InputStream inputStream, Class<T> type)
    {
        Optional<JsonElement> elementOptional = parse(inputStream);
        if (elementOptional.isPresent())
        {
            return deserialize(elementOptional.get().asJsonObject(), type);
        }
        return null;
    }

    public static <T> Optional<T>  deserialize(File file, Class<T> type)
    {
        Optional<JsonElement> elementOptional = parse(file);
        if (elementOptional.isPresent())
        {
            return deserialize(elementOptional.get().asJsonObject(), type);
        }
        return null;
    }

    public static void toFile(Object object, File file, JsonFormat format)
    {
        Optional<JsonElement> elementOptional = serialize(object);
        if (elementOptional.isPresent())
        {
            toFile(elementOptional.get().toJson(format), file);
        }
    }

    public static void toFile(Object object, File file)
    {
        toFile(object, file, JsonFormat.NORMAL);
    }

    public static void toFile(JsonElement element, JsonFormat format, File file)
    {
        toFile(format.format(element.toJson()), file);
    }

    public static void toFile(JsonElement element, File file)
    {
        toFile(element, JsonFormat.NORMAL, file);
    }

    public static void toFile(String json, File file)
    {
        FileWriter fileWriter;
        try
        {
            if (!file.exists())
            {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            fileWriter = new FileWriter(file);
            fileWriter.write(json);
            fileWriter.flush();
            fileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
